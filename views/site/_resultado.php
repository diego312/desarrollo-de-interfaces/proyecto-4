<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$titulo = 'Ciclistas de la vuelta España';
?>

<h1><?=$model -> nombre?></h1>
<p>  <?="Dorsal ". $model -> dorsal?>, <?=$model -> edad?> años, <?="Ciclista de ". $model -> nomequipo?></p>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                      <div class="card minimo">
                        <div class="card-body">
                           <h3 class="card-title"> <?=$totales ?></h3>
                           <p class="card-text">Etapas Ganadas
                                   </p>
                        </div>
                      </div>
                  </div>
            <div class="col-sm-6 col-md-3">
                      <div class="card minimo">
                        <div class="card-body">
                           <h3 class="card-title"><?=$puertos ?></h3>
                           <p class="card-text">Puertos Ganados</p>

                        </div>
                      </div>
                  </div>
            
            <div class="col-sm-6 col-md-3">
                      <div class="card minimo">
                        <div class="card-body">
                           <h3 class="card-title"><?=$maillots?></h3>
                           <p class="card-text">Maillot Ganados</p>

                        </div>
                      </div>
                  </div>
            
       <div class="col-sm-6 col-md-3">
                      <div class="card minimo">
                        <div class="card-body">
                           <h3 class="card-title"><?=$premio?>€</h3>
                           <p class="card-text">Premio Ganado</p>

                        </div>
                      </div>
                  </div>
        </div>
