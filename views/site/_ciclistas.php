<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$titulo = 'Ciclistas de la vuelta España';
?>



    
    <div class="col-sm-12">
            <div class="card minimo">
              <div class="card-body">
                 <h5 class="card-title"><?=$model -> nombre?></h5>
                 <p class="card-text"><?="Dorsal del ciclista: ". $model -> dorsal?></p>
                 <?= Html::a('Ver más',['site/estadisticas', 'dorsal'=>$model -> dorsal, ], ['class' => 'btn btn-primary btn-block'] ) ?>
              </div>
            </div>
        </div>

     
