<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div id="fondo">
    
    <h1>Vuelta Ciclista España</h1>
    
    <p> <?= Html::a('Estadísticas Generales',['site/generales'], ['class' => 'btn btn-info btn-lg btn-block'] ) ?></p>
    <p> <?= Html::a('Estadísticas De los Ciclistas',['site/ciclistas'], ['class' => 'btn btn-info btn-lg btn-block'] ) ?></p>

</div>


