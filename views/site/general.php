<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$this->title = 'Vuelta ciclista';
?>
 <?=   ListView::widget([
            'dataProvider' => $resultado,

          'layout'=>"{items}",
          'viewParams' => ['media' => $media],

        ]);
?>

  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-interval="false">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" ></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
     
      <div class="carousel-caption d-none d-md-block">
        <h5>Número de ciclistas por equipo</h5>
        <p>Gráfico del total de ciclistas que tiene cada equipo..</p>
      </div>
    </div>
    <div class="carousel-item">

      <div class="carousel-caption d-none d-md-block">
        <h5>Ciclistas y las etapas que han ganado.</h5>
        <p>Aquí iría el gráfico de las etapas ganadas por X ciclista.</p>
      </div>
    </div>
    <div class="carousel-item">

      <div class="carousel-caption d-none d-md-block">
        <h5>Ciclistas y los puertos que han ganado.</h5>
        <p>Aquí iría el gráfico de los puertos ganados por X ciclista.</p>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div>



<div class="body-content">
    <div class="row">
        
        <div class="col-sm-4">
          <div class="card alturaminima">
            <div class="card-body">
              <h5 class="card-title">La etapa <?=$numero?> con <?=$larga?> kms</h5>
        
              <p class="card-text">La etapa más larga</p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title">Maillot más llevado</h5>
                <p class="card-text">Código del maillo + número de veces que se ha llevado</p>
              </div>
            </div>
         </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title">Hay <?=$circular?> etapas circulares</h5>
                <p class="card-text">Número de etapas circulares.</p>
              </div>
            </div>
          </div>
        
    </div>
    
    <div class="row">
        <div class="col-sm-4">
          <div class="card alturaminima">
            <div class="card-body">
              <h5 class="card-title">Ciclista que ha ganado más etapas</h5>
              <p class="card-text">Nombre del ciclista + número de etapas ganadas..</p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title">Ciclista que ha ganado más puerto</h5>
                <p class="card-text">Nombre del ciclista + número de puertos ganados.</p>
              </div>
            </div>
         </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title">Ciclista que ha llevado más maillots</h5>
                <p class="card-text">Nombre del ciclista + número de maillots llevados.</p>
              </div>
            </div>
          </div>
        
    </div>
    
    <div class="row">
        <div class="col-sm-4">
          <div class="card alturaminima">
            <div class="card-body">
              <h5 class="card-title"><?=$masalto?> con <?=$altura?> m</h5>
              <p class="card-text">Puerto más alto</p>
            </div>
          </div>
        </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title">Equipo con el mayor número de ciclistas</h5>
                <p class="card-text">Nombre del equipo + número de ciclista</p>
              </div>
            </div>
         </div>
        
        <div class="col-sm-4">
            <div class="card alturaminima">
              <div class="card-body">
                <h5 class="card-title"><?=$media?> años</h5>
                <p class="card-text">Edad media de los ciclistas</p>
              </div>
            </div>
          </div>
        
    </div>
        
</div>
 
 