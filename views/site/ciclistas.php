<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$titulo = 'Ciclistas de la vuelta España';
?>


<div class="jumbotron ">
          <h1><?= $titulo?></h1>
                 
        <?=   ListView::widget([
            'dataProvider' => $dataProvider,
           'itemView' => '_ciclistas',
            'layout'=>"\n{items}\n\n\n{pager}",
            
        ]);
?>
          
      </div>
        
 