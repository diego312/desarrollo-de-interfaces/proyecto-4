<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
        <div class="body-content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            Está página muestra la información y las estadísticas basandose en la vuelva ciclista España del año 1992.
        </p>
    </div>

 
</div>
